﻿using UnityEngine;
using UnityEngine.VR.WSA.Input;

public class TapToPlaceParent : MonoBehaviour
{
    bool placing = false;
    public GameObject bookCover;
    GestureRecognizer recognizer;

    /// <summary>
    /// Called the for initiating first time
    /// </summary>
    void Start()
    {
        recognizer = new GestureRecognizer();
        recognizer.TappedEvent += (source, tapCount, ray) =>
        {
            if(placing)
            {
                placing = false;
                recognizer.StopCapturingGestures();
                Debug.Log("drag not placing inside recognizer");
                SpatialMapping.Instance.DrawVisualMeshes = false;
            }
            
        };
     }

    /// <summary>
    /// Called by GazeGestureManager when the user performs a Select gesture
    /// </summary>
    void OnSelect()
    {
        Debug.Log("drag function called");
        placing = true;

        if (placing)
        {
            Debug.Log("drag  placing");
            SpatialMapping.Instance.DrawVisualMeshes = true;
            recognizer.StartCapturingGestures();
        } else {
            Debug.Log("drag not placing");
            SpatialMapping.Instance.DrawVisualMeshes = false;
            recognizer.StopCapturingGestures();
        }
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        if (placing)
        {
            var headPosition = Camera.main.transform.position;
            var gazeDirection = Camera.main.transform.forward;
            RaycastHit hitInfo;

            if (Physics.Raycast(headPosition, gazeDirection, out hitInfo,
                30.0f, SpatialMapping.PhysicsRaycastMask))
            {

                var pt = hitInfo.point;
                pt.z = bookCover.transform.position.z;
                bookCover.transform.position = hitInfo.point;
                Quaternion toQuat = Camera.main.transform.localRotation;
                toQuat.x = 0;
                toQuat.z = 0;
                bookCover.transform.rotation = toQuat;
                bookCover.transform.Rotate(0, 270, 75);
            }
        }
    }
}