﻿using HoloToolkit;
using UnityEngine.VR.WSA.Input;
using UnityEngine;

/// <summary>
/// HandsManager keeps track of when a hand is detected.
/// </summary>
public class HandsManager : Singleton<HandsManager>
{
    [Tooltip("Audio clip to play when Finger Pressed.")]
    public AudioClip FingerPressedSound;
    private AudioSource audioSource;
    
    /// <summary>
    /// Tracks the hand detected state.
    /// </summary>
    public bool HandDetected
    {
        get;
        private set;
    }

    public GameObject FocusedGameObject { get; private set; }
    
    void Awake()
    {
        EnableAudioHapticFeedback();
        InteractionManager.SourceDetected += InteractionManager_SourceDetected;
        InteractionManager.SourceLost += InteractionManager_SourceLost;
        InteractionManager.SourcePressed += InteractionManager_SourcePressed;
        InteractionManager.SourceReleased += InteractionManager_SourceReleased;
        FocusedGameObject = null;
    }


    private void EnableAudioHapticFeedback()
    {
        if (FingerPressedSound != null)
        {
            audioSource = GetComponent<AudioSource>();
            if (audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
            }

            audioSource.clip = FingerPressedSound;
            audioSource.playOnAwake = false;
            audioSource.spatialBlend = 1;
            audioSource.dopplerLevel = 0;
        }
    }


    private void InteractionManager_SourceDetected(InteractionSourceState hand)
    {
        HandDetected = true;
    }


    private void InteractionManager_SourceLost(InteractionSourceState hand)
    {
        HandDetected = false;
        ResetFocusedGameObject();
    }


    private void InteractionManager_SourcePressed(InteractionSourceState hand)
    {
        if (InteractibleManager.Instance.FocusedGameObject != null)
        {
            if (audioSource != null && !audioSource.isPlaying &&
                (InteractibleManager.Instance.FocusedGameObject.GetComponent<Interactible>() != null &&
                InteractibleManager.Instance.FocusedGameObject.GetComponent<Interactible>().TargetFeedbackSound == null))
            {
                audioSource.Play();
            }

            FocusedGameObject = InteractibleManager.Instance.FocusedGameObject;
        }
    }


    private void InteractionManager_SourceReleased(InteractionSourceState hand)
    {
        ResetFocusedGameObject();
    }


    private void ResetFocusedGameObject()
    {
        FocusedGameObject = null;
        GestureManager.Instance.ResetGestureRecognizers();
    }


    void OnDestroy()
    {
        InteractionManager.SourceDetected -= InteractionManager_SourceDetected;
        InteractionManager.SourceLost -= InteractionManager_SourceLost;
        InteractionManager.SourceReleased -= InteractionManager_SourceReleased;
        InteractionManager.SourcePressed -= InteractionManager_SourcePressed;
    }

}